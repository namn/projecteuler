a = [x for x in xrange(1, 1000) if (x % 3) == 0 or (x % 5) == 0]
print sum(a)
assert sum(a) == 233168
