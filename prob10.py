import prob7

primes = []
for i in xrange(2, 2000000):
    if prob7.is_prime(i, primes):
        primes.append(i)
print sum(primes)
