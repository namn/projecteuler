import math

def triangle_number_generator():
    r = 0
    i = 1
    while True:
        r += i
        yield r
        i += 1

gen = triangle_number_generator()
assert next(gen) == 1
assert next(gen) == 3
assert next(gen) == 6
assert next(gen) == 10
assert next(gen) == 15
assert next(gen) == 21
assert next(gen) == 28

if __name__ == '__main__':
    gen = triangle_number_generator()
    done = False
    while not done:
        i = next(gen)
        count = 1
        j = 1
        sqrt_i = math.sqrt(i) + 1
        while j < sqrt_i:
            if (i % j) == 0:
                count += 2
                if count > 500:
                    done = True
                    break
            j += 1
    assert i == 76576500
    print i
