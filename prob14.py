def collatz_generator(begin):
    i = begin
    while True:
        yield i
        if i == 1:
            return
        if (i % 2) == 0:
            i = i // 2
        else:
            i = 3 * i + 1

def count_collatz_terms(n, count={1: 1}):
    try:
        return count[n]
    except KeyError:
        if (n % 2) == 0:
            new_n = n // 2
        else:
            new_n = 3 * n + 1
        r = 1 + count_collatz_terms(new_n, count)
        count[n] = r
        return r

if __name__ == '__main__':
    count = {1: 1}
    found_max = [1, 1]
    for i in xrange(1, 1000000):
        nr_terms = count_collatz_terms(i, count)
        if nr_terms > found_max[1]:
            found_max = [i, nr_terms]
    assert found_max[0] == 837799
    print found_max[0]
