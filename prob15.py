def solve(nr_row, nr_col):
    seen = {}
    def do_solve(nr_row, nr_col):
        if (nr_row == 1) or (nr_col == 1):
            return 1
        key = (nr_row, nr_col)
        try:
            return seen[key]
        except KeyError:
            r = do_solve(nr_row - 1, nr_col) + do_solve(nr_row, nr_col - 1)
            seen[key] = r
            return r
    nr_row_dot = nr_row + 1
    nr_col_dot = nr_col + 1
    return do_solve(nr_row_dot, nr_col_dot)
assert solve(1, 1) == 2
assert solve(2, 2) == 6
assert solve(20, 20) == 137846528820
print solve(20, 20)
