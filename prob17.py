singles = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven',
    'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen',
    'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
tens = ['_', '_', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy',
    'eighty', 'ninety']

def spell(n):
    single = n % 10
    ten = n % 100 // 10
    hundred = n % 1000 // 100
    if n == 1000:
        return 'one thousand'

    r = []
    if hundred:
        r.append(singles[hundred] + ' hundred')
        if single or ten:
            r.append(' and ')
    if ten >= 2:
        r.append(tens[ten])
        if single:
            r.append(' ')
    if ten == 1:
        r.append(singles[ten * 10 + single])
    elif single:
        r.append(singles[single])

    return ''.join(r)
        

assert spell(1) == 'one'
assert spell(2) == 'two'
assert spell(9) == 'nine'
assert spell(10) == 'ten'
assert spell(11) == 'eleven'
assert spell(12) == 'twelve'
assert spell(19) == 'nineteen'
assert spell(20) == 'twenty'
assert spell(21) == 'twenty one'
assert spell(22) == 'twenty two'
assert spell(99) == 'ninety nine'
assert spell(100) == 'one hundred'
assert spell(101) == 'one hundred and one'
assert spell(102) == 'one hundred and two'
assert spell(109) == 'one hundred and nine'
assert spell(110) == 'one hundred and ten'
assert spell(999) == 'nine hundred and ninety nine'
assert spell(1000) == 'one thousand'
assert spell(342) == 'three hundred and forty two'
assert spell(115) == 'one hundred and fifteen'

strings = [spell(x).replace(' ', '') for x in xrange(1, 1001)]
s = sum(len(s) for s in strings)
assert s == 21124
print s
