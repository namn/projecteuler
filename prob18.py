def read_triangle(filename):
    with open(filename, 'r') as f:
        inp = f.readlines()

    rows = [x.split(' ') for x in inp]
    for i, row in enumerate(rows):
        rows[i] = [int(x) for x in row]

    return rows


def solve(inp):
    found_max = [0] * len(inp)
    for i, row in enumerate(inp):
        found_max[i] = [0] * len(row)
    found_max[0][0] = inp[0][0]
    # loop through all but the last row
    for row in xrange(len(inp) - 1):
        next_row = row + 1
        for col in xrange(len(inp[row])):
            found_max[next_row][col] = max(
                found_max[next_row][col],
                found_max[row][col] + inp[next_row][col]
            )
            found_max[next_row][col + 1] = max(
                found_max[next_row][col + 1],
                found_max[row][col] + inp[next_row][col + 1]
            )
    return max(found_max[-1])


assert solve([[3], [7, 4], [2, 4, 6], [8, 5, 9, 3]]) == 23

rows = read_triangle('prob18.inp')
assert solve(rows) == 1074
print solve(rows)
