count = 0
year = 1900
month = 1
day = 1


def is_leap_year(y):
    if (y % 400) == 0:
        return True
    if (y % 100) == 0:
        return False
    if (y % 4) == 0:
        return True
    return False


nr_sundays = 0
while year <= 2000:
    count += 1
    if (count % 7) == 0:
        if year >= 1901 and day == 1:
            nr_sundays += 1
    day += 1
    max_day = 31
    if month == 2:
        max_day = 28
        if is_leap_year(year):
            max_day = 29
    elif month in (4, 6, 9, 11):
        max_day = 30
    if day > max_day:
        day = 1
        month += 1
        if month > 12:
            month = 1
            year += 1


assert nr_sundays == 171
print count, nr_sundays
