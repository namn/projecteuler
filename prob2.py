last_two = [1, 2]
sum_even = 2
while True:
    fibo = sum(last_two)
    if fibo > 4000000:
        break
    if (fibo % 2) == 0:
        sum_even += fibo
    last_two = [last_two[1], fibo]
print sum_even
