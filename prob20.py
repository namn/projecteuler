def factorial(n):
    if n == 1:
        return 1
    r = 1
    while n >= 1:
        r *= n
        n -= 1
    return r

assert factorial(1) == 1
assert factorial(2) == 2
assert factorial(3) == 6
assert factorial(4) == 24

if __name__ == '__main__':
    s = str(factorial(100))
    s = sum(ord(x) - 0x30 for x in s)
    assert s == 648
    print s
