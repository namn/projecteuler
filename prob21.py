import math

def divisors(n):
    s = set()
    i = 1
    sqrt_n = int(math.sqrt(n) + 1)
    while i < sqrt_n:
        q = n % i
        if q == 0:
            r = n // i
            s.add(r)
            s.add(i)
        i += 1
    return s

def proper_divisors(n):
    return divisors(n) - set([n])

def is_amicable(x):
    sum_dx = sum(proper_divisors(x))
    sum_dy = sum(proper_divisors(sum_dx))
    if x == sum_dy and sum_dx != x:
        return sum_dx

assert divisors(1) == set([1])
assert divisors(2) == set([1, 2])
assert divisors(3) == set([1, 3])
assert divisors(4) == set([1, 2, 4])
assert divisors(5) == set([1, 5])
assert divisors(6) == set([1, 2, 3, 6])
assert divisors(7) == set([1, 7])
assert divisors(8) == set([1, 2, 4, 8])
assert divisors(9) == set([1, 3, 9])
assert is_amicable(284) == 220
assert is_amicable(220) == 284

if __name__ == '__main__':
    s = 0
    for i in range(1, 10000):
        if is_amicable(i):
            s += i
    assert s == 31626
    print s
