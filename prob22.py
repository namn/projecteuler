with open('prob22.inp', 'r') as f:
    inp = f.read()

inp = inp.split(',')
inp.sort()

s = 0
for i, name in enumerate(inp):
    name = name.replace('"', '')
    point = sum(ord(x) - 0x40 for x in name)
    s += point * (i + 1)
assert s == 871198282
print s
