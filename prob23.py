from prob21 import divisors

def is_abundant(n):
    s = sum(divisors(n) - set([n]))
    if s > n:
        return s
    return 0

assert is_abundant(12) == 16

abundant_numbers = set()
for i in xrange(1, 28124):
    if is_abundant(i):
        abundant_numbers.add(i)

s = 0
for i in xrange(28123, 0, -1):
    for j in abundant_numbers:
        if (i > j) and (i - j) in abundant_numbers:
            break
    else:
        s += i
assert s == 4179871
print s
