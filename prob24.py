import itertools

gen = itertools.permutations([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
for i in xrange(1000000 - 1):
    next(gen)
r = next(gen)
assert r == (2, 7, 8, 3, 9, 1, 5, 4, 6, 0)
print next(gen)
