target = 10**999

def fibonacci_generator():
    f1, f2 = 1, 1
    while True:
        yield f1
        f1, f2 = f2, f1 + f2

for idx, i in enumerate(fibonacci_generator()):
    if i > target:
        break
assert idx + 1 == 4782
print idx + 1
