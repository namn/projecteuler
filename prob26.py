def find_cycle(numer, denom):
    seen_at = {}
    pos = 0
    numer = numer % denom
    while numer != 0:
        pos += 1
        if numer in seen_at:
            return pos - seen_at[numer]
        seen_at[numer] = pos
        numer *= 10
        numer = numer % denom
    return 0

assert find_cycle(1, 1) == 0
assert find_cycle(1, 2) == 0
assert find_cycle(1, 3) == 1
assert find_cycle(1, 4) == 0
assert find_cycle(1, 5) == 0
assert find_cycle(1, 6) == 1
assert find_cycle(1, 7) == 6
assert find_cycle(1, 8) == 0
assert find_cycle(1, 9) == 1
assert find_cycle(1, 10) == 0

found_max = (1, 0)
for i in xrange(1, 1000):
    length = find_cycle(1, i)
    if length > found_max[1]:
        found_max = (i, length)
assert found_max[0] == 983
print found_max[0]
