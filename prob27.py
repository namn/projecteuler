from prob7 import is_prime

# precompute some primes < 88K because 80 * 80 + 1000 * 80 + 1000 is about 88K 
primes = set([b for b in xrange(1, 88000) if is_prime(b)])
# because n**2 + a * n + b = b when n is 0, so b should be a prime
bs = [b for b in primes if b < 1000]
found_max = (30, 0)
# so, b is prime, n(n + a) should be even
# if n is even, it naturally holds,
# if n is odd, a must be odd for it to hold
for a in xrange(-999, 1000, 2):
    for b in bs:
        # n is naturally capped at 80 ;)
        for n in xrange(0, 80):
            i = n ** 2 + a * n + b
            if i not in primes:
                break
        if n > found_max[0]:
            found_max = (n, a * b)
assert found_max[1] == -59231
print found_max[1]
