# 1
# 3 5 7 9  (3 = 1 + 2, 5 = 3 + 2, sum = 9 + (9 - 2) + (9 - 4) + (9 - 6))
# 13 17 21 25 (13 = 9 + 4, 17 = 13 + 4, sum = 25*4 - 4*6)
# 31 37 43 49 (31 = 25 + 6)
s = 1
displace = 2
for i in xrange(3, 1002, 2):
    s += i**2 * 4 - displace * 6
    displace += 2
assert s == 669171001
print s
