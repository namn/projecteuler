def factorize(n):
    if n  == 1:
        return [1]
    r = []
    while n > 1:
        if (n % 2) == 0:
            r.append(2)
            n = n // 2
            continue
        for i in xrange(3, n + 1, 2):
            if (n % i) == 0:
                n = n // i
                r.append(i)
                break
    return r


assert factorize(1) == [1]
assert factorize(2) == [2]
assert factorize(3) == [3]
assert factorize(4) == [2, 2]
assert factorize(5) == [5]
assert factorize(6) == [2, 3]
assert factorize(7) == [7]
assert factorize(8) == [2, 2, 2]
assert factorize(9) == [3, 3]


if __name__ == '__main__':
    factors = factorize(600851475143L)
    print max(factors)
