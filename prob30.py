# 9**5 * 6 = 354294 => maximum 6 digits

fifth_powers = [x**5 for x in xrange(0, 10)]

def is_sum_of_fifth_power(i):
    digits = [ord(x) - 0x30 for x in str(i)]
    s = sum(fifth_powers[x] for x in digits)
    return s == i

assert is_sum_of_fifth_power(4150)
assert is_sum_of_fifth_power(4151)
assert not is_sum_of_fifth_power(4152)

s = 0
for i in xrange(2, 354292):
    if is_sum_of_fifth_power(i):
        s += i
assert s == 443839
print s
