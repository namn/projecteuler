DENOMS = [1, 2, 5, 10, 20, 50, 100, 200]
DENOMS.reverse()

def coin_change(value, denom_idx=0):
    if value <= 0 or denom_idx >= len(DENOMS):
        return 0
    denom = DENOMS[denom_idx]
    ways = 0
    accumulated = 0
    while accumulated < value:
        ways += coin_change(value - accumulated, denom_idx + 1)
        accumulated += denom
    if accumulated == value:
        ways += 1
    return ways

assert(coin_change(0) == 0)
assert(coin_change(1) == 1)
assert(coin_change(2) == 2)
assert(coin_change(3) == 2)
assert(coin_change(4) == 3)
assert(coin_change(5) == 4)
print coin_change(200)  # 73682
