def is_pandigital(x, y, z, n=9):
    x = str(x)
    y = str(y)
    z = str(z)
    s = set(x) | set(y) | set(z)
    if len(x) + len(y) + len(z) == n and \
            s == set('123456789'[ : n]) and '0' not in s:
        return True
    return False
    
pan_products = set()
for multiplicand in range(100):
    for multiplier in range(10000):
        product = multiplicand * multiplier
        if is_pandigital(multiplicand, multiplier, product):
            pan_products.add(product)

assert(is_pandigital(39, 186, 7254))
print pan_products
print sum(pan_products) # 45228
