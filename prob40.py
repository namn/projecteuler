# lazy
s = bytearray(1000001)
i = 1
pos = 1
while pos <= 1000000:
    digits = str(i)
    for j, d in enumerate(digits):
        try:
            s[pos] = digits[j]
        except IndexError:
            break
        pos += 1
    i += 1

assert s[1] == 0x31
assert s[2] == 0x32
assert s[10] == 0x31
assert s[11] == 0x30
assert s[12] == 0x31
r = 1
for i in xrange(1, 7):
    r *= s[10**i] - 0x30
assert r == 210
print r
