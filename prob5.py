from prob3 import factorize
import collections

prime_count = collections.defaultdict(int)
for i in xrange(1, 21):
    factors = factorize(i)
    local_count = collections.defaultdict(int)
    for factor in factors:
        local_count[factor] += 1
    for key in local_count:
        prime_count[key] = max(prime_count[key], local_count[key])
prod = 1
for key in prime_count:
    prod = prod * (key ** prime_count[key])
print prod
