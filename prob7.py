import math


def is_prime(n, primes=[]):
    if n == 1 or n == 2:
        return True
    if (n % 2) == 0:
        return False
    sqrt_n = math.sqrt(n) + 1
    if primes:
        for factor in primes:
            if factor > sqrt_n:
                break
            if (n % factor) == 0:
                return False
    else:
        i = 3
        while i < sqrt_n:
            if (n % i) == 0:
                return False
            i += 2
    return True

assert is_prime(1)
assert is_prime(2)
assert is_prime(3)
assert not is_prime(4)
assert is_prime(5)
assert not is_prime(6)
assert is_prime(7)
assert not is_prime(8)
assert not is_prime(9)
assert not is_prime(10)
assert is_prime(11)
assert not is_prime(12)
assert is_prime(13)
if __name__ == '__main__':
    primes = [2, 3, 5, 7, 11, 13]
    candidate = 14
    for i in xrange(6, 10001):
        while not is_prime(candidate, primes):
            candidate += 1
        primes.append(candidate)
        candidate += 1
    print primes[-1]
